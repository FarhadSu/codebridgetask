using Application;
using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using AspNetCoreRateLimit;
using Domain;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddOptions();
builder.Services.AddMemoryCache();
builder.Services.Configure<IpRateLimitOptions>(builder.Configuration.GetSection("IpRateLimiting"));
builder.Services.AddInMemoryRateLimiting();

builder.Services.AddNpgsql<CodebridgeDbContext>(builder.Configuration.GetConnectionString("Docker"), o => o.MigrationsAssembly("Domain"));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddControllers();

builder.Services.AddScoped<IDogService, DogService>(); 
builder.Services.AddScoped<IDogRepository, DogRepository>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
builder.Services.AddAutoMapper(typeof(AutomapperProfile));

var app = builder.Build();

app.UseIpRateLimiting();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.MapGet("/ping", () => "Dogs house service. Version 1.0.1");
app.MapControllers();

app.Run();