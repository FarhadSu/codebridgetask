﻿using Application.DTOs;
using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DogsController : ControllerBase
    {
        private readonly IDogService service;

        public DogsController(IDogService service)
        {
            this.service = service;
        }

        [HttpGet]
        public async ValueTask<IActionResult> GetAllDogsAsync([FromQuery] string? attribute, string? order, int? pageNumber, int? pageSize)
        {
            if (attribute is null && pageNumber is null)
            {
                var result = await service.GetAllDogsAsync();
                return result is not null ? Ok(result) : NotFound();
            }
            else
            {
                if (pageNumber is not null && pageSize is not null)
                {
                    if (pageNumber < 0 || pageSize < 0)
                    {
                        return BadRequest();
                    }

                    var result = await service.GetAllDogsAsync(pageNumber.Value, pageSize.Value);
                    return result is not null ? Ok(result) : NotFound();
                }
                else if (attribute is not null && order is not null)
                {
                    if (attribute != "tail_length" && attribute != "weight")
                    {
                        return BadRequest();
                    }
                    if (order != "desc" && order != "asc")
                    {
                        return BadRequest();
                    }

                    var result = await service.GetAllDogsAsync(attribute, order);
                    return result is not null ? Ok(result) : NotFound();
                }
                else
                {
                    return BadRequest();
                }
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddDogAsync([FromBody] DogAddDto dog)
        {
            await service.AddDogAsync(dog);
            return Ok();
        }
    }
}
