﻿using Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Domain
{
    public class CodebridgeDbContext : DbContext
    {
        public CodebridgeDbContext() { }

        public CodebridgeDbContext(DbContextOptions<CodebridgeDbContext> options) : base(options) { }

        public DbSet<Dog> Dogs { get; set; } = default!;
    }
}
