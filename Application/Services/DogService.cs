﻿using Application.DTOs;
using Application.Interfaces;
using AutoMapper;
using Domain.Model;

namespace Application.Services
{
    public class DogService : IDogService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public DogService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task AddDogAsync(DogAddDto dog)
        {
            if (unitOfWork.DogRepository.GetByNameAsync(dog.Name)?.Result is null)
            {
                var newDog = mapper.Map<Dog>(dog);
                await unitOfWork.DogRepository.AddAsync(newDog);
                await unitOfWork.SaveAsync();
            }
        }

        public async ValueTask<IEnumerable<DogDto>> GetAllDogsAsync()
        {
            var result = await unitOfWork.DogRepository.FindAllAsync();
            return mapper.Map<IEnumerable<DogDto>>(result);
        }

        public async ValueTask<IEnumerable<DogDto>> GetAllDogsAsync(string attribute, string order)
        {
            if (attribute == "weight")
            {
                var result = await unitOfWork.DogRepository.FindOrderByWeightAsync(order.Equals("desc"));
                return mapper.Map<IEnumerable<DogDto>>(result);
            }
            else
            {
                var result = await unitOfWork.DogRepository.FindOrderByLenghtAsync(order.Equals("desc"));
                return mapper.Map<IEnumerable<DogDto>>(result);
            }
        }

        public async ValueTask<IEnumerable<DogDto>> GetAllDogsAsync(int pageNumber, int pageSize)
        {
            var result = await unitOfWork.DogRepository.FindAllAsync(pageNumber, pageSize);
            return mapper.Map<IEnumerable<DogDto>>(result);
        }
    }
}
