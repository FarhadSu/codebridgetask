﻿namespace Application.DTOs
{
    public class DogAddDto
    {
        public string Name { get; set; } = default!;
        public string Color { get; set; } = default!;
        public int TailLength { get; set; } = default!;
        public int Weight { get; set; } = default!;
    }
}
