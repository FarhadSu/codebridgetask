﻿using Domain.Model;

namespace Application.Interfaces
{
    public interface IDogRepository
    {
        ValueTask<IQueryable<Dog>> FindAllAsync();
        ValueTask<IQueryable<Dog>> FindOrderByWeightAsync(bool desc);
        ValueTask<IQueryable<Dog>> FindOrderByLenghtAsync(bool desc);
        ValueTask<IQueryable<Dog>> FindAllAsync(int pageNumber, int pageSize);
        Task<Dog?> GetByNameAsync(string name);

        Task AddAsync(Dog entity);
    }
}
