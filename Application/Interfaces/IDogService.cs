﻿using Application.DTOs;

namespace Application.Interfaces
{
    public interface IDogService
    {
        ValueTask<IEnumerable<DogDto>> GetAllDogsAsync();
        ValueTask<IEnumerable<DogDto>> GetAllDogsAsync(string attribute, string order);
        ValueTask<IEnumerable<DogDto>> GetAllDogsAsync(int pageNumber, int pageSize);
        Task AddDogAsync(DogAddDto dog);
    }
}
