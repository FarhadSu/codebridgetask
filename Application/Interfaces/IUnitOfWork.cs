﻿namespace Application.Interfaces
{
    public interface IUnitOfWork
    {
        IDogRepository DogRepository { get; }

        Task<int> SaveAsync();
    }
}
