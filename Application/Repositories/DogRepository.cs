﻿using Application.Interfaces;
using Domain;
using Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Application.Repositories
{
    public class DogRepository : IDogRepository
    {
        private readonly DbSet<Dog> dbSet;

        public DogRepository(CodebridgeDbContext context)
        {
            dbSet = context.Set<Dog>();
        }

        public async Task AddAsync(Dog entity)
        {
            await dbSet.AddAsync(entity);
        }

        public async Task<Dog?> GetByNameAsync(string name)
        {
            return await dbSet.FirstOrDefaultAsync(x => x.Name == name);
        }


        public async ValueTask<IQueryable<Dog>> FindAllAsync()
        {
            return await Task.Run(() => dbSet.AsNoTracking());
        }

        public async ValueTask<IQueryable<Dog>> FindOrderByWeightAsync(bool desc)
        {
            if (desc)
            {
                return await Task.Run(() => dbSet.AsNoTracking().OrderByDescending(x => x.Weight));
            }
            else
            {
                return await Task.Run(() => dbSet.AsNoTracking().OrderBy(x => x.Weight));
            }
        }

        public async ValueTask<IQueryable<Dog>> FindOrderByLenghtAsync(bool desc)
        {
            if (desc)
            {
                return await Task.Run(() => dbSet.AsNoTracking().OrderByDescending(x => x.TailLength));
            }
            else
            {
                return await Task.Run(() => dbSet.AsNoTracking().OrderBy(x => x.TailLength));
            }
            
        }


        public async ValueTask<IQueryable<Dog>> FindAllAsync(int pageNumber, int pageSize)
        {
            return await Task.Run(() => dbSet.AsNoTracking().Skip(pageNumber).Take(pageSize));
        }
    }
}
