﻿using Application.Interfaces;
using Domain;

namespace Application.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CodebridgeDbContext context;
        private IDogRepository dogRepository = default!;

        public UnitOfWork(CodebridgeDbContext context)
        {
            this.context = context;
        }

        public IDogRepository DogRepository => dogRepository ??= new DogRepository(context);

        public async Task<int> SaveAsync()
        {
            return await context.SaveChangesAsync();
        }
    }
}
